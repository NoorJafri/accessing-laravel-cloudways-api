<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel Cloudways Api Example
                </div>

                <div class="links">
                    <a href="http://phplaravel-14171-57029-159086.cloudwaysapps.com/listServer">List Servers</a>
                    <a href="http://phplaravel-14171-57029-159086.cloudwaysapps.com/createServer">Create Server</a>
                </div>
                <div class="links">
                    <form action="http://phplaravel-14171-57029-159086.cloudwaysapps.com/stopServer" method="GET">
                     <br>
                      Stop Server: <input placeholder="Server ID" type="text" name="serverId">
                      <input type="submit" value="Stop">
                     <br>
                    </form>
                    <form action="http://phplaravel-14171-57029-159086.cloudwaysapps.com/restartServer" method="GET">
                     <br>
                      Restart Server: <input placeholder="Server ID" type="text" name="serverId">
                      <input type="submit" value="Restart">
                     <br>
                    </form>
                    <form action="http://phplaravel-14171-57029-159086.cloudwaysapps.com/restartVarnish" method="GET">
                     <br>
                      Restart Varnish: <input placeholder="Server ID" type="text" name="serverId">
                      <input type="submit" value="Restart Var.">
                     <br>
                    </form>
                    <form action="http://phplaravel-14171-57029-159086.cloudwaysapps.com/restartNginx" method="GET">
                     <br>
                      Restart Nginx: <input placeholder="Server ID" type="text" name="serverId">
                      <input type="submit" value="Restart Nginx.">
                     <br>
                    </form>
                    <form action="http://phplaravel-14171-57029-159086.cloudwaysapps.com/listServerSettings" method="GET">
                     <br>
                      Server Settings: <input placeholder="Server ID" type="text" name="serverId">
                      <input type="submit" value="Settings">
                     <br>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
