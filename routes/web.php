<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/*use Illuminate\Http\Request;*/


Route::get('/', function () {
    return view('welcome');
});



Route::get('/createServer',"Server@createServer");
Route::get('/listServer',"Server@listServer");
Route::get('/stopServer',"Server@stopServer");
Route::get('/restartServer',"Server@restartServer");
Route::get('/restartVarnish',"Server@restartVarnish");
Route::get('/restartNginx',"Server@restartNginx");
Route::get('/listServerSettings',"Server@listServerSettings");

/*
Route::get('/stopServer', function (Request $request) {
    $serverId = $request->input('serverId');
    return $serverId;
});*/