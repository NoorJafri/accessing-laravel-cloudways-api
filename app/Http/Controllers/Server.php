<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Server extends BaseController
{

    public function callCloudwaysAPI($method, $url, $accessToken, $post = []){

        $baseURL = getenv('BASE_URL');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_URL, $baseURL . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        //Set Authorization Header
        if ($accessToken) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $accessToken]);
        }
        //Set Post Parameters
        $encoded = '';
        if (count($post)) {
            foreach ($post as $name => $value) {
                $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
            }
            $encoded = substr($encoded, 0, strlen($encoded) - 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode != '200') {
            die('An error occurred code: ' . $httpcode . ' output: ' . substr($output, 0, 10000));
        }
        curl_close($ch);
        return json_decode($output);
    }

    public function accessToken (){
        $api_key = getenv('API_KEY');
        $api_email = getenv('API_EMAIL');
        $tokenResponse = $this->callCloudwaysAPI('POST', '/oauth/access_token',null,['email'=>$api_email,
                                                                                     'api_key'=>$api_key,]);
        $accessToken = $tokenResponse->access_token;
        return ($accessToken);
    }

    public function createServer (){
        $accessToken = $this->accessToken();
        $addSeverResponse = $this->callCloudWaysAPI('POST', '/server', $accessToken, [
        'cloud' => 'do',
        'region' => 'nyc3',
        'instance_type' => '512MB',
        'application' => 'wordpress',
        'app_version' => '4.6.1',
        'server_label' => 'API Test',
        'app_label' => 'API Test',
        ]);
        
        $operation = $addSeverResponse->server->operations[0];
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';


    }

    public function listServer(){
        $accessToken = $this->accessToken();
        $addSeverResponse = $this->callCloudWaysAPI('GET', '/server', $accessToken,null);
        echo '<pre>';
        print_r($addSeverResponse->servers);
        echo  '</pre>';
    }

    public function stopServer(Request $request){
        $accessToken = $this->accessToken();
        $serverId = $request->input('serverId');
        $addSeverResponse = $this->callCloudWaysAPI('POST', '/server/stop', $accessToken, [
        'server_id' => $serverId
        ]);
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';
    }

    public function restartServer(Request $request){
        $accessToken = $this->accessToken();
        $serverId = $request->input('serverId');
        $addSeverResponse = $this->callCloudWaysAPI('POST', '/server/restart', $accessToken, [
        'server_id' => $serverId
        ]);
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';
    }

    public function restartVarnish(Request $request){
        $accessToken = $this->accessToken();
        $serverId = $request->input('serverId');
        $addSeverResponse = $this->callCloudWaysAPI('POST', '/service/state', $accessToken, [
        'server_id' => $serverId,
        'service' => 'varnish',
        'state' => 'restart'
        ]);
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';
    }

    public function restartNginx(Request $request){
        $accessToken = $this->accessToken();
        $serverId = $request->input('serverId');
        $addSeverResponse = $this->callCloudWaysAPI('POST', '/service/state', $accessToken, [
        'server_id' => $serverId,
        'service' => 'nginx',
        'state' => 'restart'
        ]);
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';
    }

    public function listServerSettings(Request $request){
        $accessToken = $this->accessToken();
        $serverId = $request->input('serverId');
        $addSeverResponse = $this->callCloudWaysAPI('GET', '/server/manage/settings?server_id=' . $serverId, $accessToken, []);
        echo '<pre>';
        print_r($addSeverResponse);
        echo  '</pre>';
    }


 
}
